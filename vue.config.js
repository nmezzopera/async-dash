module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/async-dash/' : '/',
  transpileDependencies: ['vuetify'],
  pluginOptions: {
    env: {
      GITLAB_APP_CLIENT_ID:
        process.env.GITLAB_APP_CLIENT_ID ||
        '829df4785045a70c62a3a55c81689b9b231698e195af355d1880a8b552d197f9',
      GITLAB_APP_REDIRECT_URI:
        process.env.GITLAB_APP_REDIRECT_URI || 'http://localhost:8080/oauth-callback',
    },
  },
};
