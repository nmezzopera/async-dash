import { getResource } from '@/utils/caciotta';
import { orderBy } from 'lodash';

export const state = () => ({
  list: [],
  details: {},
  issuesDetails: {},
});

export const mutations = {
  SET_DATA(state, { key, value }) {
    state[key] = value;
  },
  ADD_PERSON(state, person) {
    state.list.push(person);
  },
  REMOVE_PERSON(state, index) {
    const [deleted] = state.list.splice(index, 1);
    state.details = { ...state.details, [deleted.id]: undefined };
  },
  SET_DETAILS(state, { data, id }) {
    state.details = { ...state.details, [id]: { ...state.details[id], ...data } };
  },
  SET_ISSUE_DETAILS(state, { data, iid }) {
    state.issuesDetails = {
      ...state.issuesDetails,
      [iid]: data,
    };
  },
};

export const actions = {
  addPerson({ commit }, person) {
    commit('ADD_PERSON', person);
  },
  removePerson({ commit }, index) {
    commit('REMOVE_PERSON', index);
  },
  async fetchPersonDetails({ commit, dispatch }, assignee_id) {
    const issueParams = {
      assignee_id,
      scope: 'all',
      per_page: 100,
    };
    const issueStatistics = await getResource('/issues_statistics', {
      params: issueParams,
    });
    const issues = await getResource('/issues/', { params: issueParams });

    const mrs = await getResource('/merge_requests', { params: issueParams });
    const data = { ...issueStatistics.data, issues: issues.data, mrs: mrs.data };

    commit('SET_DETAILS', { data, id: assignee_id });
    issues.data.forEach(i => dispatch('fetchIssueDetails', i));
  },
  async fetchIssueDetails({ commit }, { iid, project_id }) {
    const { data } = await getResource(`/projects/${project_id}/issues/${iid}/notes`, {
      params: {
        sort: 'desc',
        order_by: 'updated_at',
        per_page: 100,
      },
    });

    commit('SET_ISSUE_DETAILS', { iid, data });
  },
};

export const getters = {
  getPersonDetails: state => id => {
    const base = state.list.find(m => `${m.id}` === `${id}`);
    return { ...base, ...state.details[id] };
  },
  getTimelineData: state => id => {
    const person = state.details[id];
    if (!person) {
      return [];
    }
    const filtered = person.issues.reduce((a, c) => {
      const notes = state.issuesDetails[c.iid];
      if (notes) {
        const asyncUpdates = notes.filter(i => i.body.match(/.*async.*update.*/gim));
        a.push(
          ...asyncUpdates.map(au => ({
            ...au,
            body: au.body.replace('## Async Issue Update\n\n', ''),
            issue: { link: c.web_url, title: c.title },
          })),
        );
      }
      return a;
    }, []);
    return orderBy(filtered, 'created_at', 'desc');
  },
};

export const namespaced = true;
