import ClientOAuth2 from 'client-oauth2';
import { getResource } from '@/utils/caciotta';

const oauth = new ClientOAuth2({
  clientId: process.env.GITLAB_APP_CLIENT_ID,
  authorizationUri: 'https://gitlab.com/oauth/authorize',
  redirectUri: process.env.GITLAB_APP_REDIRECT_URI,
  scopes: ['read_api', 'read_user', 'profile', 'openid'],
});

export const state = () => ({
  accessToken: null,
  user: null,
});

export const mutations = {
  SET_DATA(state, { key, value }) {
    state[key] = value;
  },
};

export const actions = {
  startLogin() {
    window.location.href = oauth.token.getUri();
  },
  setAccessToken({ commit }, value) {
    commit('SET_DATA', { key: 'accessToken', value });
  },
  async fetchUser({ commit }) {
    const { data } = await getResource('/user');
    commit('SET_DATA', { key: 'user', value: data });
  },
};

export const namespaced = true;
