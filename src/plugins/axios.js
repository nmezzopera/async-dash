import axios from 'axios';
import store from '@/store/';

const instance = axios.create({
  baseURL: 'https://gitlab.com/api/v4/',
  transformRequest: [
    (data, headers) => {
      headers.common.Authorization = `Bearer ${store.state.auth.accessToken}`;
      return data;
    },
  ],
});

export default instance;
