import axios from '@/plugins/axios';
const caciotta = {};

const EXPIRE_TIME = 10 * 60 * 1000;

export const simpleHash = value => {
  let hash = 0;
  let chr;

  for (let i = 0; i < value.length; i++) {
    chr = value.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0;
  }
  return hash;
};

export const refetch = key =>
  !caciotta[key] || Date.now() - caciotta[key].lastExecuted > EXPIRE_TIME;

export const getResource = (resource, params) => {
  const key = simpleHash(resource + JSON.stringify(params));
  if (refetch(key)) {
    caciotta[key] = {
      lastExecuted: Date.now(),
      resource: axios.get(resource, params),
    };
  }

  return caciotta[key].resource;
};
